using VDS.RDF.Query;
using Vue2Spa.Facades;

namespace Vue2Spa.Models
{
    public class Monument
    {
        public string Name { get; set; }        
        public string BuildingStartDate { get; set; }        
        public string BuildingEndDate { get; set; }        
        public string Author { get; set; }        
        public string Location { get; set; }        
        public int AnnualVisitors { get; set; }        
        public string Description { get; set; }        
        public string Image { get; set; }        

        public Monument(SparqlResult sparqlResult)
        {
            Name = SparqlFacade.GetProperty(sparqlResult, "name");
            BuildingStartDate = SparqlFacade.GetProperty(sparqlResult, "buildingStartDate");
            BuildingEndDate = SparqlFacade.GetProperty(sparqlResult, "buildingEndDate");
            Author = SparqlFacade.GetProperty(sparqlResult, "architect");
            Location = SparqlFacade.GetProperty(sparqlResult, "location");
            AnnualVisitors = SparqlFacade.GetDecimalProperty(sparqlResult, "annualVisitors");
            Description = SparqlFacade.GetProperty(sparqlResult, "description");
            Image = SparqlFacade.GetProperty(sparqlResult, "image");
        }
    }
}
