using System;
using System.Collections.Generic;
using VDS.RDF.Query;
using Vue2Spa.Facades;

namespace Vue2Spa.Models
{
    public class Country
    {
        public string Name { get; set; }        
        public string Capital { get; set; }
        public Decimal Area { get; set; }
        public int PopulationTotal { get; set; }
        public string Demonym { get; set; }
        public string Currency { get; set; }
        public string Flag { get; set; }
        public string Motto { get; set; }
        public string ImageMap { get; set; }
        public HashSet<Monument> Monuments { get; set; }        

        public Country(SparqlResult sparqlResult)
        {
            Name = SparqlFacade.GetProperty(sparqlResult, "name");
            Capital = SparqlFacade.GetProperty(sparqlResult, "capital");
            Area = Convert.ToDecimal(SparqlFacade.GetProperty(sparqlResult, "area"));
            PopulationTotal = SparqlFacade.GetDecimalProperty(sparqlResult, "populationTotal");
            Demonym = SparqlFacade.GetProperty(sparqlResult, "demonym");
            Currency = SparqlFacade.GetProperty(sparqlResult, "currency");
            Flag = SparqlFacade.GetProperty(sparqlResult, "flag");
            Motto = SparqlFacade.GetProperty(sparqlResult, "motto");
            ImageMap = SparqlFacade.GetProperty(sparqlResult, "imageMap");
            HashSet<Monument> mons = SparqlFacade.GetMonuments(this);
            Monuments = mons;
        }
    }
}
