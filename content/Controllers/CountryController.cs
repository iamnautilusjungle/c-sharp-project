using Microsoft.AspNetCore.Mvc;
using Vue2Spa.Models;
using Vue2Spa.Facades;

namespace Vue2Spa.Controllers
{
    [Route("api/[controller]")]
    public class CountryController : Controller
    {
        [Route("[action]")]
        public Country show([FromQuery(Name = "country")] string country)
        {
            return SparqlFacade.GetCountry(country);
        }
    }
}
