using System.Runtime.CompilerServices;
using System.Collections.Generic;
using VDS.RDF.Query;
using Vue2Spa.Models;
using System;
using System.Linq;
namespace Vue2Spa.Facades
{
    public class SparqlFacade
    {
        /// <summary>
        /// Get the used endpoint.
        /// </summary>
        private static SparqlRemoteEndpoint GetEndpoint()
        {
            return new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"), "http://dbpedia.org");
        }

        private static string FormatCountryName(string country)
        {
            return country.Replace("_", " ");
        }
        
        /// <summary>
        /// Returns a country for the given country name.
        /// </summary>
        public static Country GetCountry(string country) {
            SparqlRemoteEndpoint endpoint = GetEndpoint();

            //Make a SELECT query against the Endpoint
            SparqlResultSet results = endpoint.QueryWithResultSet(@"
            SELECT ?x str(?name) as ?name str(?capitalName) as ?capital str(?area) as ?area str(COALESCE(?popTot, ?popCensus)) as ?populationTotal str(?demonym) as ?demonym str(?currency) as ?currency ?flag str(COALESCE(?englishMotto, ?commonMotto)) as ?motto str(?imageMap) as ?imageMap
            WHERE {

            # Basic where
            ?x a dbo:PopulatedPlace .
            ?x rdfs:label ?name .

            # Capitals
            OPTIONAL { ?x dbo:capital ?capitalObject} .
            OPTIONAL { ?capitalObject rdfs:label ?capitalName } .

            # Area
            OPTIONAL { ?x dbo:areaTotal ?area } .

            # Currency
            OPTIONAL { ?x dbo:currency ?currencyUri } .
            OPTIONAL { ?currencyUri rdfs:label ?currency } .

            # Population
            OPTIONAL { ?x dbo:PopulationTotal ?popTot } .
            OPTIONAL { ?x dbp:populationCensus ?popCensus } .
            OPTIONAL { ?x dbo:demonym ?demonym } .

            # Flag
            OPTIONAL { ?x dbo:thumbnail ?flag } .

            # Map
            OPTIONAL { ?x dbp:imageMap ?imageMap } .

            # Motto
            OPTIONAL { ?x dbo:motto ?commonMotto } .
            OPTIONAL { ?x dbp:englishmotto ?englishMotto } .

            # Filters
            FILTER (?name = '"+FormatCountryName(country)+@"'@en)

            } LIMIT 1
            ");

            Country countryObject = new Country(results.First());

            countryObject.Monuments = SparqlFacade.GetMonuments(countryObject);

            return countryObject;
        }

        /// <summary>
        /// Returns the monuments of a given country.
        /// </summary>
        public static HashSet<Monument> GetMonuments(Country country)
        {
            HashSet<Monument> monuments = new HashSet<Monument>();

            SparqlRemoteEndpoint endpoint = GetEndpoint();

            //Make a SELECT query against the Endpoint
            SparqlResultSet results = endpoint.QueryWithResultSet(@"
            SELECT DISTINCT ?x str(?name) as ?name str(?buildingStartDate) as ?buildingStartDate str(?buildingEndDate) as ?buildingEndDate str(?architect) as ?architect str(?location) as ?location str(?description) as ?description str(?image) as ?image
            WHERE {
                # Basic where
                ?x a dbo:Building .
                ?x rdfs:label ?name .
				?x dbo:location/rdfs:label ?locName .
                ?x dbo:buildingEndDate ?buildingEndDate .
                ?x dbo:buildingStartDate ?buildingStartDate .
                ?x dbo:architect/rdfs:label ?architect .
                ?x dbo:location/rdfs:label ?location .
				?x dbo:thumbnail ?image .
                ?x dbo:abstract ?description .
				
                # Filters
                FILTER (?locName = '"+country.Name+@"'@en)
				FILTER(lang(?name) = 'en')
				FILTER(lang(?architect) = 'en')
				FILTER(lang(?location) = 'en')
				FILTER(lang(?description) = 'en')
            } LIMIT 100
            ");

			HashSet<string> names = new HashSet<string>();

            foreach (SparqlResult item in results)
            {
				Monument m = new Monument(item);
				if(!names.Contains(m.Name)) {
					monuments.Add(m);
					names.Add(m.Name);
				}
            }

            return monuments;
        }

        /// <summary>
        /// Simple handler to avoid bugs.
        /// </summary>
        public static string GetProperty(SparqlResult sparqlResult, string property)
        {
            return sparqlResult.HasBoundValue(property) ? sparqlResult.Value(property).ToString() : null;
        }

        public static int GetDecimalProperty(SparqlResult sparqlResult, string property)
        {
            return Convert.ToInt32(GetProperty(sparqlResult, property));
        }
    }
}
