import HomePage from 'components/home-page'
import WorldMap from 'components/world-map'
import FetchData from 'components/fetch-data'
import About from 'components/about'
import Country from 'components/country'

export const routes = [
  { name: 'home', path: '/', component: HomePage, display: 'Home', icon: 'home' },
  { name: 'about', path: '/about', component: About, display: 'About Template', icon: 'info' },
  { name: 'world', path: '/around-the-world', component: WorldMap, display: 'World', icon: 'graduation-cap' },
  { name: 'country', path: '/countries/:country', component: Country, display: 'World', icon: 'graduation-cap' },
  { name: 'fetch-data', path: '/fetch-data', component: FetchData, display: 'Data', icon: 'list' }
]
